" Plugins
call plug#begin()
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'godlygeek/tabular'
Plug 'altercation/vim-colors-solarized'
Plug 'ntpeters/vim-better-whitespace'
Plug 'leafgarland/typescript-vim'
Plug 'rust-lang/rust.vim'
Plug 'rstacruz/vim-closer'
Plug 'mbbill/undotree'
Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}
Plug 'mhinz/vim-grepper'
" Plug 'majutsushi/tagbar'

call plug#end()

" Plugin config
set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 0
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0
let g:ctrlp_working_path_mode = 'ra'
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='base16'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
let g:ctrlp_dont_split = 'NERD'
let g:strip_whitespace_on_save = 0
let g:NERDSpaceDelims = 1
let mapleader = ","

" Basic autocmds
" autocmd vimenter * NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" Random configs
cd ~
set tabstop=4
set softtabstop=0
set expandtab
set shiftwidth=4
set smarttab
set backspace=2
set encoding=utf-8
set relativenumber
set ruler
set backupdir=~/vimfiles/backup//
set directory=~/vimfiles/swap//
set undodir=~/vimfiles/undo//
set colorcolumn=80
set updatetime=1000
set autoread
set synmaxcol=140

" GUI options
syntax enable
set background=dark
colorscheme solarized
if has('gui_running')
    highlight SignColumn guibg=#073642
    set signcolumn=yes
    set guifont=Fira\ Code\ 9
	set guioptions-=m
	set guioptions-=T
	set guioptions-=r
	set guioptions-=L
endif

" Key
inoremap <C-BS> <C-W>
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv
noremap <C-n> :NERDTreeToggle<CR>
noremap <C-b> :UndotreeToggle<CR>
nnoremap <C-tab> :tabnext<CR>
inoremap <C-tab> <Esc>:tabnext<CR>
nnoremap <C-S-tab> :tabprevious<CR>
inoremap <C-S-tab> <Esc>:tabprevious<CR>
" nnoremap <C-i> :buffers<CR>:buffer<Space>
nnoremap <C-c> :bp\|bd #<CR>
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
map <silent><C-\> <Plug>NERDCommenterToggle
inoremap <C-Enter> <Esc>O
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" inoremap <expr> <cr>    pumvisible() ? "\<C-y>" : "\<cr>"
noremap <C-S-f> :Grepper -tool rg<CR>
nmap <F12> <Plug>(coc-definition)
nmap <M-F12> <Plug>(coc-references)
map <C-m> <Plug>(coc-codeaction)
map <C-`> <Esc>:vert term<CR>
" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()
